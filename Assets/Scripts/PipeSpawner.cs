﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour
{
    // Global Variables
    [SerializeField] private Bird bird;
    [SerializeField] private Pipe pipeUp, pipeDown;
    [SerializeField] private Point point;
    [SerializeField] private PipeHoleTrigger pipeHoleTrigger;
    [SerializeField] private float spawnInterval = 1f;
    [SerializeField] public float holeSize = 1f;
    [SerializeField] private float maxMinOffset = 1f;

    private Coroutine CR_Spawn;

    // Start is called before the first frame update
    void Start()
    {
        // Memulai Spawning
        StartSpawn();
    }

    void StartSpawn()
    {
        // Menjalankan Fungsi Coroutine IeSpawn()
        if(CR_Spawn == null)
        {
            CR_Spawn = StartCoroutine(IeSpawn());
        }
    }

    void StopSpawn()
    {
        // Menghentikan Coroutine IeSpawn jika sebelumnya sudah
        // di jalankan
        if(CR_Spawn != null)
        {
            StopCoroutine(CR_Spawn);
        }
    }

    void SpawnPipe()
    {
        // Menduplikasi game objek pipeUp dan menempatkan posisinya 
        // sama dengan game objek ini tetapi dirotasi 180 derajat
        Pipe newPipeUp = Instantiate(pipeUp, transform.position,
            Quaternion.identity);

        // Mengaktifkan game objek newPipeUp
        newPipeUp.gameObject.SetActive(true);

        // Menduplikasi game objek pipeDown dan menempatkan posisinya 
        // sama dengan game objek ini
        Pipe newPipeDown = Instantiate(pipeDown, transform.position,
            Quaternion.identity);

        // Mengaktifkan game objek newPipeUp
        newPipeDown.gameObject.SetActive(true);

        // Menempatkan posisi dari pipa yang sudah terbentuk agar
        // memiliki lubang di tengahnya
        newPipeUp.transform.position += Vector3.up * (holeSize / 2);
        newPipeDown.transform.position += Vector3.down * (holeSize / 2);

        float y = maxMinOffset * Mathf.Sin(Time.time);
        newPipeUp.transform.position += Vector3.up * y;
        newPipeDown.transform.position += Vector3.up * y;

        Point newPoint = Instantiate(point, transform.position,
            Quaternion.identity);
        newPoint.gameObject.SetActive(true);
        newPoint.SetSize(holeSize);
        newPoint.transform.position += Vector3.up * y;

        PipeHoleTrigger newPipeHoleTrigger = Instantiate(pipeHoleTrigger,
            pipeHoleTrigger.transform.position, pipeHoleTrigger.transform.rotation);
        newPipeHoleTrigger.gameObject.SetActive(true);
        newPipeHoleTrigger.AssignPipe(newPipeUp, newPipeDown);

    }

    IEnumerator IeSpawn()
    {
        while (true)
        {
            // Jika Burung mati maka menghentikan pembuatan Pipa Baru
            if (bird.IsDead())
            {
                StopSpawn();
            }

            // Membuat Pipa Baru
            SpawnPipe();

            // Menunggu beberapa detik sesuai dengan spawn interval
            yield return new WaitForSeconds(spawnInterval);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
