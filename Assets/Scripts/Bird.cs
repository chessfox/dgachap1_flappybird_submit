﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Bird : MonoBehaviour
{
    // Global Variables
    [SerializeField] private float upForce = 100f;
    [SerializeField] private bool isDead;
    [SerializeField] private int score = 0;
    [SerializeField] private Text scoreText;
    [SerializeField] private UnityEvent OnJump, OnDead;
    [SerializeField] private UnityEvent OnAddPoint;
    private Rigidbody2D rb;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        scoreText.text = score.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if(!isDead && Input.GetMouseButtonDown(0))
        {
            Jump();
        }
    }

    public bool IsDead()
    {
        return isDead;
    }

    public void Dead()
    {
        if(!isDead && OnDead != null)
        {
            OnDead.Invoke();
        }

        isDead = true;
    }

    void Jump()
    {
        if (rb)
        {
            rb.velocity = Vector2.zero;
            rb.AddForce(new Vector2(0, upForce));
        }

        if(OnJump != null)
        {
            OnJump.Invoke();
        }
    }

    public void AddScore(int value)
    {
        // Menambahkan Score value
        score += value;

        // Pengecekan Null Value
        if (OnAddPoint != null)
        {
            // Memanggil semua event pada OnAddPoint
            OnAddPoint.Invoke();
        }

        // Mengubah nilai text pada score text
        scoreText.text = score.ToString();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        animator.enabled = false;
    }
}
