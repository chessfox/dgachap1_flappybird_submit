﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private RectTransform Title;
    [SerializeField] private RectTransform Tap;
    [SerializeField] private GameObject Grounds;
    [SerializeField] private Ease easeType;

    // Start is called before the first frame update
    void Start()
    {
        Grounds.transform.DOMoveY(-4.7f, 0.5f)
            .SetEase(easeType)
            .OnComplete(() =>
            {
                //-5.2378f
                Grounds.transform.DOMoveY(-5.956571f, 0.5f);
            }
            );
        Tap.DOAnchorPosY(-11f, 2f).SetEase(easeType);
        Title.DOAnchorPosY(103.7f, 2f).SetEase(easeType);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
