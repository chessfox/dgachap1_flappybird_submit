﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeHoleTrigger : MonoBehaviour
{
    [SerializeField] private Bird bird;
    private Pipe pipeUp;
    private Pipe pipeDown;
    private float speed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!bird.IsDead())
        {
            // Membuat pipa bergerak ke sebelah kiri dengan kecepatan tertentu
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
    }

    public void AssignPipe(Pipe newPipeUp, Pipe newPipeDown)
    {
        pipeUp = newPipeUp;
        pipeDown = newPipeDown;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Bird bird = collision.gameObject.GetComponent<Bird>();

        if(bird != null)
        {
            float holeSize = Random.Range(-0.3f, 1.5f);
            if (pipeUp.isActiveAndEnabled)
            pipeUp.transform.DOMoveY(pipeUp.transform.position.y + holeSize, 1f)
                .SetEase(Ease.InBounce);
            if (pipeDown.isActiveAndEnabled)
            pipeDown.transform.DOMoveY(pipeDown.transform.position.y - holeSize, 1f)
                .SetEase(Ease.InBounce);
        }
    }
}
