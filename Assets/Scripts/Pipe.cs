﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour
{
    // Global Variable
    [SerializeField] private Bird bird;
    [SerializeField] private float speed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!bird.IsDead())
        {
            // Membuat pipa bergerak ke sebelah kiri dengan kecepatan tertentu
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
    }

    // Membuat Bird mati ketika bersentuhan dan menjatuhkannya ke ground
    // jika mengenai di atas collider
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("bird"))
        {
            Bird bird = collision.gameObject.GetComponent<Bird>();

            // Pengecekan Null value
            if (bird)
            {
                // Mendapatkan komponen Collider pada game object
                Collider2D collider = GetComponent<Collider2D>();

                if (collider)
                {
                    collider.enabled = false;
                }

                //Burung Mati
                bird.Dead();
            }
        }
        
        if (collision.gameObject.CompareTag("bullet"))
        {
            if (CompareTag("Up"))
            { 
                transform.DOMoveY(8f, 2f).SetEase(Ease.InBounce);
            }
            else if (CompareTag("Down"))
            {
                transform.DOMoveY(-8f, 2f).SetEase(Ease.InBounce);
            }
            GetComponent<Collider2D>().enabled = false;
            Destroy(collision.gameObject);

            //gameObject.SetActive(false);
        }
    }
}
